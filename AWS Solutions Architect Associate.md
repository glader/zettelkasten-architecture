# AWS Solutions Architect Associate
- https://www.udemy.com/course/aws-certified-solutions-architect-associate-saa-c02/learn/lecture/13528014#overview
- https://www.knowledgehut.com/practice-tests/aws-solutions-architect-associate


### How I Took the AWS Solutions Architect Professional Exam
1. If the question and answers filled the screen, I immediately skipped it. I didn’t read it or scan it; I moved on. That way I didn’t duplicate the time spent reading a question that I would come back to and read later.
2. I then read and answered all of the other smaller questions in the order presented on the test.
3. If I couldn’t quickly identify an answer (and feel good about it), I’d take my best guess and flag the question for review later. While some people might be tempted to leave it unanswered, supplying a “best guess” at the answer worked better. It gave me an idea as to “where my head was” when I came back to it at the end. Plus, if I started running short on time, I would have at least selected an answer.
4. Once I went through all 75 questions, the test automatically took me back to the first unanswered one. I used this time to reassess how much time and unanswered questions I had left. As an aside, every time that I have taken the test I’ve usually skipped about six questions because of their length.
5. Lastly, I answered the long questions — and then reviewed the questions about which I was unsure after answering every other question.

---
- [[AWS]]