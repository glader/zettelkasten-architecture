# API Gateway
- Supports WebSocket
- Handle API versioning (v1, v2)
- Handle different environments (dev, test, prod)
- Authentication and authorization
- Requests throttling
- Swagger / Open API
- Generate SDK specifications
- Cache API responses

Authorization:
- Sig v4 - checking permissions in IAM (good for users already within AWS account)
- Lambda Authorizer (Custom Authorizer) (good for 3rd party tokens, paid)
- [[Cognito]]

---
- [[AWS]]