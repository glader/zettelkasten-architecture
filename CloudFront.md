# CloudFront
Content Delivery Network (CDN). Based on edge locations.

Origin Access Identity - allows S3 bucket to communicate only with CloudFront.

CloudFront can be used to upload files to S3.

GeoRestriction:
- WhiteList (by countries)
- BlackList (by countries)

Price classes
- All - all regions
- 200 - all but excludes the most expensive regions
- 100 - only the least expensive regions

Origin groups - for failover. If primary origin fails, the second one is used.

---
- [[AWS]]