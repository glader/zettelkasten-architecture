# Load balancing
Tool to automatically redirect requests between servers.

## AWS
- Elastic Load Balancer (ELB)
	- Classic load balancer (CLB)
		- deprecated
	- Application load balancer (ALB)
		- can work with HTTP, HTTPS, WebSocket
		- send traffic to target groups
			- EC2 instances
			- ECS tasks
			- [[Lambda]] functions
			- IP address
	- Networ load balancer (NLB)
		- can work with TCP, TLS, UDP
	- Gateway load balancer (GWLB)
		- operates with IP
		- send all traffic to a target group, which can analyze it, drop or pass
		- all cleared traffic is sent to an application
		- uses GENEVE protocol on port 6081

Stickiness by cookies (session affinity).

### Cross Zone load balancing
- Application load balancer
	- Always on
	- No charges for inter AZ traffic

- Network load balancer
	- Disabled by default
	- You pay charges for inter AZ traffic

- Classic load balances
	- Disables by default
	- No charges for inter AZ traffic

### Deregistration delay (connection draining)
Time to complete "in-flight" requests while the instance become unhealthy.

---
- [[AWS]]