# Server name indication (SNI)
Web server can provide several web sites. Client should have a posibility to provide hostname during [[SSL]] handshake, so server can choose correct certificate.

![[sni.png]]