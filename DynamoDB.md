# DynamoDB
[[Serverless]] nosql database.
- Fully managed by Amazon
- Scales to massive workloads
- Distributed
- Fast and consistent
- Auto-scaling

Each table has a Primary Key.
Each item has attributes (like [[MongoDB]]). Maximum item size is 400Kb.
Supported types:
- Scalar types - string, number, binary, boolean, Null
- Document types - list, map
- Set types - string set, number set, binary set

Provisioned mode (default):
- you can specify the number of reads/writes per second
- pay for provisioned Read Capacity Units (RCU) & Write Capacity Units (WCU)

On-demand mode:
- reads/writes automatically scale
- no capacity planning
- pay for what you use
- more expensive

DynamoDB Accelerator (DAX)
- In-memory cache for DynamoDB.
- TTL - 5 minutes

DynamoDB Streams
- Ordered stream of item-level events (create/update/delete).

DynamoDB global tables
- The same table, accessible in different regions. Two-way replication.

Time to live (TTL)
- Automatically delete rows after expiration

Transactions

---
- [[AWS]]
- [[Databases]]