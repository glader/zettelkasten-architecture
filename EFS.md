# EFS
Elastic File System.
- can be mounted to many EC2
- multi-AZ
- expensive (3x gp2)
- pay per use (no need to provision)
- 10 GB/s throughput

Performance mode:
- general purpose
- max i/o

Throughput mode:
- bursting (1 TB = 50 MiB/s + burst of up to 100 MiB/s)
- provisioned (can be set up)

---
- [[AWS]]