# Neptune
Fully managed graph database.

- Highly available across 3 AZ
- 15 read replicas
- Backup to [[S3]]

---
- [[AWS]]
- [[Databases]]