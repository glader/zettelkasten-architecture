# Useful resources
- https://github.com/donnemartin/system-design-primer
- https://www.youtube.com/c/HighLoadChannel/videos
- AWS re:Invent
- https://t.me/it_arch
- https://t.me/architectureweekly
- https://vvsevolodovich.dev/
- https://t.me/system_design_interviews
- https://t.me/its_reading_club
- https://aws.amazon.com/ru/solutions/
- https://github.com/vvsevolodovich/solution-architect-roadmap
- https://www.youtube.com/c/SystemDesignInterview/videos

### Books
- https://architectelevator.com/architecture/architect-bookshelf/
- designing data-intensive applications
- foundation of software architecture
- documenting software architecture
- https://www.piter.com/product/system-design-podgotovka-k-slozhnomu-intervyu
- https://www.goodreads.com/book/show/15814648-cloud-architecture-patterns
- https://www.goodreads.com/book/show/3828902-thinking-in-systems
- https://www.goodreads.com/book/show/17255186-the-phoenix-project

### Tools
- https://github.com/mingrammer/diagrams - diagrams as a code