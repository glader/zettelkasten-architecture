# Storage Gateway
It's a bridge between [[AWS]] storages and on-premise data.

- File gateway
	- S3 accessible via NFS and SMB protocols
	- Data cached in gateway
	- Can be mounted on many servers
	- Integrated with Active Directory
- Volume gateway
	- Block storage via iSCSI
	- Cached volumes: low latency
	- Stored volumes: dataset is on premise, backup to S3
- Tape gateway
	- Virtual Tape Library
	- Compatible with most popular backup software
- Hardware Aplliance - server to install in your data-center