# Serverless
It's a service, where you shouldn't manage virtual server to run it.

## AWS
- [[S3]]
- [[Lambda]]
- [[Fargate]]
- [[DynamoDB]]
- [[Cognito]]
- [[SQS]]
- [[SNS]]
- [[Aurora]] serverless
- [[Kinesis]]
- Step Functions
- [[API Gateway]]