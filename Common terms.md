# Common terms
- Scalability - system can handle greater loads by adapting
- Vertical scalability - increase size of single element (scale up / down)
- Horisontal scalability - increase amount of identical elements (scale out / in)
- Elasticity = horisontal scalability
- High avalability - some amount of actions and risk preventions
	- auto scaling (active HA)
	- auto recovery
	- servers in different data centers (passive HA)