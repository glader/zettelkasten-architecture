# Databases
## AWS
SQL (OLTP). Good for structured data, for joins.
- [[RDS]]
- [[Aurora]]

NoSQL.
- [[DynamoDB]] (json)
- [[ElastiCache]] (key/value)
- [[Neptune]] (graph)

Object store
- [[S3]] / Glacier

Data warehouses. SQL analytics, BI.
- [[Redshift]] (OLTP, column-based)
- [[Athena]]

Search.
- [[OpenSearch]] (json)

## Other
Column-based
- [[Clickhouse]]

NoSQL
- [[MongoDB]]
