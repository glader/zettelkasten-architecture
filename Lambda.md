# Lambda
[[Serverless]] functions.

Lambda@Edge - run function on each Edge location. It can modify user's request or server's response.

## Limits
**Deployment**
- Deployment size: 50Mb
- Uncompressed size: 250Mb

**Execution**
- Memory: 128Mb - 10Gb
- Execution time: 15 minutes
- Environment variables: 4Kb
- Disk capacity: 512Mb (`/tmp`)
- Concurrency: 1000 executions

## Integrations (triggers)
- [[API Gateway]] - for REST interface
- [[Kinesis]]
- [[DynamoDB]]
- [[S3]]
- [[CloudFront]]
- [[EventBridge]]
- CloudWatch logs
- [[SNS]]
- [[SQS]]
- [[Cognito]]
