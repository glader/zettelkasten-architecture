# Redshift
Based on [[PostgreSQL]]. It's OLAP (not OLTP). SQL-based. Analytics / BI / Data warehouse.

- 10x better perfomarnce than other data warehouses
- Columnar storage of data
- Massively Parallel Query Execution (MPP)
- 1 to 128 nodes
- Up to 128 Tb of space per node
- Placed in one AZ

Nodes
- leader node - for  query planning
- compute node - for performing queries

Sources
- [[Kinesis]] Data Firehose
- From [[S3]] using COPY command (manually) 
- From [[EC2]] via JDBC drives

Redshift Spectrum - query data in S3 without loading into Redshift.
![[redshift spectrum.png]]

GreenPlum?

---
- [[AWS]]
- [[Databases]]