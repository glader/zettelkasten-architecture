# AWS
Amazon Web Services - cloud hosting.

### Services
ECR - container registry
[[AWS ECS|ECS]] - container service
KMS - key management service
AMI - amazon machine images
SQS - simple queue service
CodeCommit - github
SWF - simple workflow service
SNS - simple notification service (email and sms)
DMS - database migration service
SMS - server migration service
ELB - elastic load balancing
[[Glue]] - etl service
EMR - elastic map reduce
Kinesis - stearming data
WAF - web application firewall
HSM - hardware security module
GuardDuty - threat detection and security monitoring for malicious or unauthorized behavior
Config - provides AWS resource inventory
SageMaker  - platform to build, train, and deploy machine learning models
Shield Advanced - protection from DDoS
CloudFormation - IaaS (deployment)
Neptune - graph database
[[Athena]] - analyze S3 via SQL
Transfer Family - FTP to S3 or EFS
ASG - Auto scaling group
Fargate - serverless container platform
SAM - [[Serverless]] Application Model. Config for [[Lambda]], [[DynamoDB]], [[API Gateway]] and [[Cognito]] user pool in one yaml file.