# FSx
Emulating Lustre or Windows Server file system.

- Scratch FS
	- Temporary storage
	- Data is not replicated
	- High burst
	- Available from other AZ
	- Usage: short-term processes
- Persistent FS
	- Long-term storage
	- Data is replicates within same AZ
	- Usage: long-term processes, sensitive data

---
- [[AWS]]