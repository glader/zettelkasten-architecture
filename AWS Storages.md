# AWS Storages
- [[S3]] - object storage
- Glacier - object archival
- [[EBS]] - network block storage
- [[EFS]] - network file system for linux servers, POSIX compatible
- [[FSx]] for Windows - network file system for Windows servers
- [[FSx]] for Lustre - high performance linux file system
- Instance storage - physical storage for EC2 (high IOPS)
- [[Storage Gateway]] - bridge between on-premise servers and S3/EFS

---
- [[AWS]]