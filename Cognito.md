# Cognito
Tool for authentication (not for authorization).

**Cognito Users Pool**
- Integrate with [[API Gateway]]
- Simple login - user and password
- Can verify emals and phone numbers and add MFA
- Allow login through Google/FB/etc
- Sends back JWT

**Cognito (Federated) Identity Pool**
- Provide AWS credentials to users so they can access AWS resources directly

**Cognito Sync**
- Synchorize data from device to Cognito


Federated Identity Pool. Cognito can create temporary credentials with STS.
![[fip.png]]






---
- [[AWS]]