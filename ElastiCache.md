# ElastiCache
Managed Redis or Memcache.

Redis
- Multi AZ with failover
- Read replicas
- Backup and restore
- One thread
- Can use token to access data
- Support SSL in flight

Memcached
- Multi-node sharding
- Not persistent
- No backup
- Multi threaded
- SASL based authentication

All of them
- Don't support IAM authentication

---
- [[AWS]]
- [[Databases]]