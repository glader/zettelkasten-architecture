# Elastic Beanstalk
Developer centric view of deploying applications on [[AWS]].

Components:
- Application: collection of EB components (environment, versions, configurations)
- Application version: an iteration of code (semver for example)
- Environment: collection of AWS resources  running an application version

---
- [[AWS]]