# Athena
Serverless SQL analyzing of files on S3. Available file formats - CSV, JSON, ORC, Avro, Parquet.

[How to analyze logs](https://aws.amazon.com/ru/premiumsupport/knowledge-center/analyze-logs-athena/)

---
- [[AWS]]
- [[Databases]]