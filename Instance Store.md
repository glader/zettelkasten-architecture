# Instance Store
Ephemeral hard disk drive for [[EC2]]. Always destroyed when an instance is terminated.

- Provides the best disk I/O performance
- Good for local cache

---
- [[AWS]]