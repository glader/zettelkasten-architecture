# Aurora
AWS proprietary DB.
- compatible with Postgres and MySQL drivers
- 3x faster than Postgres
- Can have 15 replicas (auto scaling)
- Sub 10ms replica lag
- HA native
- Cost 20% more than RDS
- 4 copies out of 6 for write approve
- 3 copies out of 6 for reading
- Shared storage between replicas
- Writer endpoint - dns record pointing to master
- Reader endpoint - dns record pointing to load balancer in front of replicas
- Global database - replications to up to 5 regions
- Integration with ML
	- SageMaker
	- Comprehend
- Can be serverless
- Can be multi-master

![[aurora.png]]

---
- [[AWS]]
- [[Databases]]