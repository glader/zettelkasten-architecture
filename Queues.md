# Queues
### AWS
SQS - queue model
SNS - pub/sub model
Kinesis - real-time streaming model
Amazon MQ - managed Apache ActiveMQ (old protocols like MQTT, AMQP)

Encryption:
- in-flight encryption using HTTPS
- at-rest encryption using [[KMS]]

#### SQS
Access contols by IAM + SQS access policies.

SQS can be accessible from other AWS accounts, by setting policies.

Message visibility timeout - message locked after polling and is not available for other workers.

Dead letter queue. If message is being processed many times, maybe something wrong with it. And we should process it separately.

Long polling - when consumer asks for new messages, queue can wait for them a bit, is queue is empty right now. It decreases a number of API calls and increases efficiency.

SQS temporary queue client

![[sqs request response.png]]

![[sqs + asg.png]]

#### SNS
Simple Notification Service. Cansend one message to several services:
- Kinesis FireHose
- SQS
- [[Lambda]]
- Email
- Email-JSON
- HTTP
- HTTPS
- SMS

#### Kinesis
Collect, process and analyze streaming data in realtime.

- Kinesis Data Streams: capture, process, and store data streams
- Kinesis Firehose: load data into AWS data stores. Packs incoming data into batches ans sends them to destination. Can transform data by [[Lambda]] functions.
- Kinesis Data Analytics: analyze data streams with SQL or Apache Flink
- Kinesis Video Streams: capture, process, and store video streams