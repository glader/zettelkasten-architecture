# Route 53
A - maps hostname to IPv4
AAAA - maps hostname to IPv6
CNAME - maps hostname to another hostname

How to connect dns record to load balancer:
- CNAME: app.mydomain.com -> lb1-1234.us-east-1.elb.amazonaws.com (works only for non-root domains, not for "mydomain.com")
- Alias: mydomain.com -> something.amazonaws.com (works for all domains)
	- specific for Route 53
	- free of charge
	- healthcheck
	- targets:
		- Load balancer
		- CloudFront
		- API Gateway
		- Elastic Beanstalk
		- S3 website
		- VPC interface endpoint
		- Global Accelerator
		- Route 53 record

### Routing policies
- Simple
- Weighted
- Failover
- Latency based
	- Nearest server by latency
- Geolocation
	- Specified continents, countries, US states
- Multi-value answer
	- Randomly choosed by a client
	- Can use healthchecks
	- Up to 8 records
- Geoproximity

---
- [[AWS]]