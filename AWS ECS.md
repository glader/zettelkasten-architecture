# AWS ECS
It's a kubernetes analogue, made by AWS.

ECS launch tupes:
- EC2. You should provision a bunch on EC2 instances, and AWS will run containers on those instances.
- Fargate. You don't need to provision EC2, it's serverless.

#### IAM roles for ECS
![[iam roles for ecs.png]]

