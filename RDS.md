# RDS
Aurora, Postgres, MySQL, MSSQL or Oracle, run in managed environment. You must provision an EC2 instance.

Ports:
- PostgreSQL: 5432
- MySQL: 3306
- Oracle RDS: 1521
- MSSQL Server: 1433
- MariaDB: 3306 (same as MySQL)
- Aurora: 5432 (if PostgreSQL compatible) or 3306 (if MySQL compatible)

Features
- Up to 5 read replicas (15 for Aurora).
- Security via IAM, security groups, [[KMS]]
- Backup / Snapshot / Point in time restore

---
- [[AWS]]
- [[Databases]]